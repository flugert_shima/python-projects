Function that checks if a given number is even and outputs True or odd and outputs False

def is_even():
    num = int(input('Enter a number: '))

    if num % 2 == 0:
        return True
    else:
        return False

is_even()



Function to check if a number is a palindrome

def is_palindrome():
    num = int(input('Enter a number: '))
    temp = num
    rev = 0

    while num > 0:
        dig = num % 10
        rev = rev * 10 + dig
        num = num//10
    if temp == rev:
        print("The number is a palindrome!")
    else:
        print("The number is not a palindrome")

is_palindrome()


Write a function to check if a number is a power of 2
import math


def is_power():
    num = int(input('Enter a number: '))

    if num <= 1:
        print('It is not a power of two')

    while num > 1:
        if math.sqrt(num) % 2 == 0:
            print('This number is a power of 2')
            break
        else:
            print('This number is not a power of 2')
            break

is_power()


Given a list iterate it and display numbers which are divisible by 5 and if you find number greater than 150 stop the loop iteration

def list_multiples(list1):

    for i in list1:
        if i % 5 == 0 and i <= 150:
            print(i)

list_multiples([5,10,15,20,30,50,150,230])



